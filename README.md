# ics-ans-sync-folders

Fetches workspace from LCR to ws-10 (remote host) through nextcloud.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
